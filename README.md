# CSV tools inspired by csvkit

Inspired by: https://github.com/jeroenjanssens/command-line-tools-for-data-science and csvkit for python.  But implemented in bash, works in Cygwin (where python is fragile).