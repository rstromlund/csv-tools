#!/usr/bin/env bash
#
# sample: Output lines from stdin to stdout with a given probability for a given duration, and with a given delay between lines.
#
# Example usage:
# $ seq 100 | sample -r 10 -d 0.5
#
# Inspired by: https://github.com/jeroenjanssens/command-line-tools-for-data-science
# And by: csvkit for python

typeset rate=100
typeset delay=0

typeset opt="e.g. -r -d"

while getopts ":r:d:" opt; do
	case ${opt} in
		(r)
			rate=${OPTARG%\%}
			;;
		(d)
			delay=${OPTARG}
			;;
		(:)
			echo "Invalid option: -${OPTARG} requires an argument" 1>&2
			exit 2
			;;
		(*)
			echo "Invalid option: -${OPTARG}" 1>&2
			exit 2
			;;
	esac
done
shift $(( OPTIND - 1 ))

if (( rate <= 0 || rate > 100 )) || [[ "${delay:-}" != [0-9]* ]] ; then
	echo -e "USAGE: ${0} [-r 99(%)] [-d delay]\n\t-r RATE : between 0 and 100 (percent sign is optional)\n\t-d DELAY : NUMBER[SUFFIX] (see man sleep)"
	exit 1
fi

# $RANDOM max value == 32767 (per `man bash`); calculate our "rate" as a percent of that.  Removes the math calculation below:
typeset probability=$(bc <<< "32767 * ${rate} / 100")

## Loop over <stdin>

typeset l='Line read from <stdin>.'
while read l ; do
	if (( RANDOM <= probability )); then
		echo "${l}"
		[[ '0' == "${delay:-}" ]] || sleep ${delay}
	fi
done

exit ${?}
