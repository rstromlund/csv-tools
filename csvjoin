#!/usr/bin/env bash
#
# csvjoin: Filter and truncate CSV files. Like the Unix "cut" command, but for tabular data.
#
# Example usage:
# $ csvjoin -d, -c species <(body tail -n+48 < z_iris.csv | head) z_irismeta.csv
#
# Inspired by: https://github.com/jeroenjanssens/command-line-tools-for-data-science
# And by: csvkit for python

#set -x
typeset delim=','
typeset cols=''

typeset opt="e.g. -c"

while getopts ":d:c:t" opt; do
	case ${opt} in
		(d)
			delim="${OPTARG}"
			;;
		(t)
			delim='	'
			;;
		([Cc])
			cols="${OPTARG}"
			;;
		(:)
			echo "Invalid option: -${OPTARG} requires an argument" 1>&2
			exit 2
			;;
		(*)
			echo "Invalid option: -${OPTARG}" 1>&2
			exit 2
			;;
	esac
done
shift $(( OPTIND - 1 ))

if [[ '' == "${cols:-}" || '' == "${delim:-}" ]] ; then
	echo "USAGE: ${0} [-d delimiter] -c column[,column]*"
	exit 1
fi

#Ensure the specified columns(s) come first and join the results:
join --header --nocheck-order -t "${delim}" <(cols -d "${delim}" -c "${cols}" cat < "${1}" | body sort) <(cols -d "${delim}" -c "${cols}" cat < "${2}" | body sort)
exit ${?}
